package xyz.stromgard.kafkaservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by mike on 7/23/17.
 */
@Controller
public class MessageController {

    private KafkaProducer producer;

    @Autowired
    public MessageController(final KafkaProducer producer) {
        this.producer = producer;
    }

    @RequestMapping(path = "send", method = RequestMethod.GET)
    @ResponseBody
    public String sendMessage(final String message) {

        for (int i = 0; i < 10000; i++) {
            this.producer.sendMessage(message + i);
        }

        System.out.println(String.format("Sent message [%s]", message));

        return "Sent message";
    }
}
